# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file :
1. converts images to tensorflow tfrecord files for training deep learning model
"""

import tensorflow as tf
import cv2
import numpy as np
from random import shuffle
import glob
import sys
import h5py

# datasets_normal_path = '../datasets/1st_data/00/*.jpg'
# datasets_abnormal_path = '../datasets/1st_data/[!00]*/*.jpg'
datasets_multi_path = '../datasets/Multi_Class/*/'
datasets_normal_path = '../datasets/*_data/00/*.jpg'
datasets_abnormal_path = '../datasets/*_data/*[!00]*/*.jpg'


def load_image(path):
    """
    returns a image with 224x224

    :param path: image path
    :return: converted image
    """

    img = cv2.imread(path)
    img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_CUBIC)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # img = img.astype(np.float32)

    return img


def resize_image(img, size=(224,224)):
    """
    returns a resized image

    :param img:
    :param size:
    :return:
    """

    img = cv2.resize(img, size, interpolation=cv2.INTER_CUBIC)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    return img


def _int_64_feature(value):
    """

    :param value:
    :return:
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    """

    :param value:
    :return:
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def get_datasets_with_label(data_path, label):
    """
    returns 2 lists (dataset and label)
    makes paired tuple (data and label)

    :param data_path:
    :param label:
    :return: 2 lists
    """

    data_addrs = glob.glob(data_path)
    data_labels = [label] * len(data_addrs)

    # zip(addrs, labels) - slice addrs and labels one by one ex) [1,2,3],[4,5,6]) -> (1, 4), (2, 5), (3, 6)
    c = list(zip(data_addrs, data_labels))

    # unpacking c
    data_addrs, data_labels = zip(*c)

    return data_addrs, data_labels


def get_datasets_without_label(data_path):
    """
    specify directory as a label trough the path
    returns 2 lists (dataset and label)
    makes paired tuple (data and label)

    :param data_path:
    :return: 2 lists
    """
    path_addrs = glob.glob(data_path)

    data_addrs = []
    data_labels = []
    cnt = 0

    for addr in path_addrs:
        # label = addr.replace('\\', '/').split('/')[-2]
        label = cnt
        data_addr, data_label = get_datasets_with_label(addr+'*.jpg', label)
        data_addrs += list(data_addr)
        data_labels += list(data_label)
        cnt += 1
        print(f'addr: {addr}, label: {label}')

    return data_addrs, data_labels


def shuffle_datasets(addrs, labels):
    """
    shuffles datasets

    :param addrs:
    :param labels:
    :return:
    """
    # zip(addrs, labels) - slice addrs and labels one by one ex) [1,2,3],[4,5,6]) -> (1, 4), (2, 5), (3, 6)
    c = list(zip(addrs, labels))
    shuffle(c)
    # unpacking c
    addrs, labels = zip(*c)

    return addrs, labels


def split_datasets(addrs, labels, ratio_train=0.7, ratio_val=0.2):
    """
    splits datasets into 3 parts for training, testing, validating

    :param labels:
    :param addrs:
    :param ratio_train: ration_test would be (1 - ration_train + ration_val)
    :param ratio_val:
    :return:
    """

def _int_64_feature(value):
    """

    :param value:
    :return:
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    """

    :param value:
    :return:
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def get_datasets_with_label(data_path, label):
    """
    returns 2 lists (dataset and label)
    makes paired tuple (data and label)

    :param data_path:
    :param label:
    :return: 2 lists
    """

    data_addrs = glob.glob(data_path)
    data_labels = [label] * len(data_addrs)

    # zip(addrs, labels) - slice addrs and labels one by one ex) [1,2,3],[4,5,6]) -> (1, 4), (2, 5), (3, 6)
    c = list(zip(data_addrs, data_labels))

    # unpacking c
    data_addrs, data_labels = zip(*c)

    return data_addrs, data_labels


def get_datasets_without_label(data_path):
    """
    specify directory as a label trough the path
    returns 2 lists (dataset and label)
    makes paired tuple (data and label)

    :param data_path:
    :return: 2 lists
    """
    path_addrs = glob.glob(data_path)

    data_addrs = []
    data_labels = []
    cnt = 0

    for addr in path_addrs:
        # label = addr.replace('\\', '/').split('/')[-2]
        label = cnt
        data_addr, data_label = get_datasets_with_label(addr+'*.jpg', label)
        data_addrs += list(data_addr)
        data_labels += list(data_label)
        cnt += 1
        # print(f'addr: {addr}, label: {label}')

    return data_addrs, data_labels


def shuffle_datasets(addrs, labels):
    """
    shuffles datasets

    :param addrs:
    :param labels:
    :return:
    """
    # zip(addrs, labels) - slice addrs and labels one by one ex) [1,2,3],[4,5,6]) -> (1, 4), (2, 5), (3, 6)
    c = list(zip(addrs, labels))
    shuffle(c)
    # unpacking c
    addrs, labels = zip(*c)

    return addrs, labels


def split_datasets(addrs, labels, ratio_train=0.7, ratio_val=0.2):
    """
    splits datasets into 3 parts for training, testing, validating

    :param labels:
    :param addrs:
    :param ratio_train: ration_test would be (1 - ration_train + ration_val)
    :param ratio_val:
    :return:
    """

    # divide datasets into 3 parts using ratio
    out_train_datasets = addrs[0: int(ratio_train * len(addrs))]
    out_train_labels = labels[0: int(ratio_train * len(labels))]
    out_val_datasets, out_val_labels = [], []
    out_test_datasets, out_test_labels = [], []

    if ratio_val > 0:
        out_val_datasets = addrs[int(ratio_train * len(addrs)): int((ratio_train + ratio_val) * len(addrs))]
        out_val_labels = labels[int(ratio_train * len(labels)): int((ratio_train + ratio_val) * len(labels))]

    if 1 - ratio_train + ratio_val > 0:
        out_test_datasets = addrs[int((ratio_train + ratio_val) * len(addrs)):]
        out_test_labels = labels[int((ratio_train + ratio_val) * len(labels)):]

    return (out_train_datasets, out_train_labels), (out_val_datasets, out_val_labels), \
           (out_test_datasets, out_test_labels)


def convert_to_tfrecord(data_addrs, data_labels, data_class='bin', data_kind='train'):
    """
    builds tfrecord formatted datasets with data and labels

    :param data_class:
    :param data_addrs:
    :param data_labels:
    :param data_kind:
    :return:
    """

    file_prefix = 'sdd'
    file_ext = '.tfrecord'
    dataset_path = '../datasets/tfrecord/' + data_class + '/'

    writer = tf.python_io.TFRecordWriter(dataset_path + file_prefix + '_' + data_class + '_' + data_kind + file_ext)

    for i in range(len(data_addrs)):
        if not i % 1000:
            print(f'{data_kind} data: {i}/{data_addrs}')
            sys.stdout.flush()

        image = load_image(data_addrs[i])
        encoded_image_string = cv2.imencode('.jpg', image)[1].tostring()
        image_name = str.encode(data_addrs[i].replace('\\', '/').split('/')[-1])

        label = data_labels[i]

        feature = {
            'image/encoded': _bytes_feature(tf.compat.as_bytes(encoded_image_string)),
            'image/format': _bytes_feature(b'JPEG'),
            'image/filename': _bytes_feature(image_name),
            'image/class/label': _int_64_feature(label),
            'image/height': _int_64_feature(224),
            'image/width': _int_64_feature(224)
        }

        example = tf.train.Example(features=tf.train.Features(feature=feature))

        writer.write(example.SerializeToString())

    writer.close()
    sys.stdout.flush()


def convert_to_hdf5(data_addrs, data_labels, data_class='bin', data_kind='train'):
    """
    builds hdf5 formatted datasets with data and labels

    :param data_class:
    :param data_addrs:
    :param data_labels:
    :param data_kind:
    :return:
    """

    file_prefix = 'sdd'
    file_ext = '.h5'
    dataset_path = '../datasets/hdf5/' + data_class + '/'
    image_shape = (len(data_addrs), 224, 224, 3)
    label_shape = (len(data_addrs),)
    keys_to_img = 'image/encoded'
    keys_to_label = 'image/class/label'
    keys_to_name = 'image/filename'

    dt = h5py.special_dtype(vlen=str)
    hdf5_file = h5py.File(dataset_path + file_prefix + '_' + data_class + '_' + data_kind + file_ext, mode='w')
    hdf5_file.create_dataset(keys_to_img, image_shape, np.int8)
    hdf5_file.create_dataset(keys_to_label, label_shape, np.int8)
    hdf5_file.create_dataset(keys_to_name, label_shape, dtype=dt)

    for i in range(len(data_addrs)):
        if not i % 1000:
            print(f'[Info] - convert_to_hdf5 - data_kind: {data_kind}, data: {i}/{data_addrs[i]}')
            #sys.stdout.flush()

        image = load_image(data_addrs[i])
        hdf5_file[keys_to_img][i, ...] = image[None]
        hdf5_file[keys_to_name][i, ...] = str.encode(data_addrs[i].replace('\\', '/').split('/')[-1])
        hdf5_file[keys_to_label][i, ...] = int(data_labels[i])

    hdf5_file.close()
    sys.stdout.flush()


def make_bin_datasets(is_tfrecord=False):
    """
    builds datasets for binary classification
    :param is_tfrecord:  if true then return tfrecord formatted datasets else hdf5 formatted datasets
    :return:
    """
    dataset_normal_addrs, dataset_normal_labels = get_datasets_with_label(datasets_normal_path, 1)
    dataset_abnormal_addrs, dataset_abnormal_labels = get_datasets_with_label(datasets_abnormal_path, 0)

    dataset_addrs = dataset_normal_addrs + dataset_abnormal_addrs
    dataset_labels = dataset_normal_labels + dataset_abnormal_labels

    dataset_addrs, dataset_labels = shuffle_datasets(dataset_addrs, dataset_labels)

    dataset_train, dataset_val, dataset_test = split_datasets(dataset_addrs, dataset_labels)

    if is_tfrecord:
        convert_to_tfrecord(dataset_train[0], dataset_train[1])
        convert_to_tfrecord(dataset_val[0], dataset_val[1], data_kind='validation')
        convert_to_tfrecord(dataset_test[0], dataset_test[1], data_kind='test')
    else:
        convert_to_hdf5(dataset_train[0], dataset_train[1])
        convert_to_hdf5(dataset_val[0], dataset_val[1], data_kind='validation')
        convert_to_hdf5(dataset_test[0], dataset_test[1], data_kind='test')

    print('*'*100)
    print(f'[Info] - datasets - train: {len(dataset_train[0])}, '
          f'valid: {len(dataset_val[0])}, test: {len(dataset_test[0])}')
    print('*'*100)


def make_multi_datasets(is_tfrecord=False):
    """
    builds datasets for multiple class classification

    :param is_tfrecord:  if true then return tfrecord formatted datasets else hdf5 formatted datasets
    :return:
    """
    dataset_addrs, dataset_labels = get_datasets_without_label(datasets_multi_path)
    dataset_addrs, dataset_labels = shuffle_datasets(dataset_addrs, dataset_labels)

    dataset_train, dataset_val, dataset_test = split_datasets(dataset_addrs, dataset_labels)

    if is_tfrecord:
        convert_to_tfrecord(dataset_train[0], dataset_train[1], data_class='multi')
        convert_to_tfrecord(dataset_val[0], dataset_val[1], data_class='multi', data_kind='validation')
        convert_to_tfrecord(dataset_test[0], dataset_test[1], data_class='multi', data_kind='test')
    else:
        convert_to_hdf5(dataset_train[0], dataset_train[1], data_class='multi')
        convert_to_hdf5(dataset_val[0], dataset_val[1], data_class='multi', data_kind='validation')
        convert_to_hdf5(dataset_test[0], dataset_test[1], data_class='multi', data_kind='test')


# make_bin_datasets()
