# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. 
2. 
"""

import tensorflow as tf
import tensorflow.contrib.slim as slim
from datasets import coil as sdd_record_dataset
from utils import dataset_utils
from models import vgg16conv as vgg

# parameter
tfrecords_path = '../tfrecords/bin'
dataset_name = 'sdd'
num_classes = 2
batch_size = 32
img_width = img_height = 224
split_name = 'train'
checkpoint_dir = '../checkpoints/bin/train'

# hyper parameter
learning_rate = 0.0002


def train(is_binary=True):
    global final_loss
    tf.logging.set_verbosity(tf.logging.INFO)

    # load dataset
    tfrecord_dataset = sdd_record_dataset.TFRecordDataset(tfrecords_path=tfrecords_path,
                                                          dataset_name=dataset_name,
                                                          num_classes=num_classes)

    # selects the 'train' dataset.
    dataset = tfrecord_dataset.get_split(split_name=split_name)
    images, labels, filenames, count = dataset_utils.load_batch(dataset, batch_size=batch_size, height=img_height, width=img_width)

    # label: one-hot encoding
    labels = slim.one_hot_encoding(labels, num_classes)

    # make the model - vgg16
    with slim.arg_scope(vgg.vgg_arg_scope()):
        logits, end_points = vgg.vgg_16(inputs=images, num_classes=num_classes, is_training=True)

    # add loss function to the graph
    if is_binary:
        tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=labels)
        # slim.losses.sigmoid_cross_entropy_with_logits(logits, labels)
    else:
        tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=labels)
        # slim.losses.softmax_cross_entropy(logits, labels)

    # total loss = user's loss + any regularization losses
    total_loss = tf.losses.get_total_loss()

    # create summaries to visualize the training process
    tf.summary.scalar('losses/Total_Loss', total_loss)

    # specify the optimizer and create the train op
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = slim.learning.create_train_op(total_loss, optimizer)

    # train
    final_loss = slim.learning.train(train_op=train_op,
                                     logdir=checkpoint_dir,
                                     number_of_steps=300,
                                     save_summaries_secs=5,
                                     save_interval_secs=600)

    print(f'Finished training. Final batch loss {final_loss: .5f}')
    print(f'Checkpoint saved in {checkpoint_dir}')


train(is_binary=True)


