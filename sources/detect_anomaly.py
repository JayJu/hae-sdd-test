# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. distinguishes whether the input image has defects of not
2. detects cases of defects through the input image
"""

import cv2
from preprocess import subtract_roi as proc
import matplotlib.pyplot as plt
import predict_keras as prd
import convert_dataset as cvt
from utils import common_util as util
from preprocess import subtract_roi as sub

result_path = '../results/'
file_path = None


def detect(img_path):
    """
    detects defects 2 times (1: binary, 2: multi class)
    show the areas if there are defects in the input image

    :param img_path:
    :return:
    """
    global file_path

    if img_path:
        file_path = img_path

    _, img_fg = sub.get_foreground(file_path)  # remove backgrounds
    img_rs = cvt.resize_image(img_fg)  # resize image to 224 x 224
    tmp_file_name = util.get_file_name(file_path)
    tmp_path = f'{result_path + tmp_file_name}'
    cv2.imwrite(tmp_path, img_rs)
    file_path = tmp_path

    is_normal = prd.predict(is_binary=True, img_path=file_path)  # predicts binary classification (normal or defect)

    file_name, file_ext = util.get_file_name(file_path).split('.')

    plt.figure(figsize=(6, 4))
    if not is_normal:
        img_rois, img_rec = proc.get_rois(file_path)
        plot_rows = len(img_rois) + 1
        plt.subplot(plot_rows, 1, 1)
        plt.imshow(img_rec)
        plt.title('Source')
        cv2.imwrite(f'{result_path + file_name}_SRC.{file_ext}', img_rec)

        for i in range(len(img_rois)):
            roi_file_path = f'{result_path + file_name}_ROI_{i}.{file_ext}'
            cv2.imwrite(roi_file_path, img_rois[i])
            y_class = prd.predict(is_binary=False, img_path=roi_file_path)
            plt.subplot(plot_rows, 1, i+2)
            plt.imshow(img_rois[i])
            plt.title(f'ROI[{i}]: {y_class}')

    else:
        plt.subplot(111)
        plt.imshow(img_rs)
        plt.title('Source')

    plt.show()


def check_args(args):
    """
    checks arguments are valid

    :param args:
    :return:
    """

    global file_path

    if not len(args) == 2:
        print(f'''
        Please check your input arguments.
        Usage: detect_anomaly.py image_path
        ''')
        exit()

    if '.jp' in args[1]:
        file_path = args[1]
    else:
        print(f'''
        Input error..
        index: 1, need: *.jpeg|*.jpg, found: {args[1]}
        ''')
        exit()

    return True


detect('D:/02.Dev/Source/hae-sdd-test/datasets/02-24-sample.jpg')
