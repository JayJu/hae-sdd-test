# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. trains model of binary class classification for detecting defects
2. trains model of multi class classification for classifying categories of defects
"""

from keras.utils.io_utils import HDF5Matrix
from tensorflow.python.keras.utils import to_categorical
from tensorflow.python.keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard
from tensorflow.python.keras.optimizers import Adam
# from tensorflow.python.keras.applications import VGG16
from models.sdd import SDDFCN

file_prefix = 'sdd'
file_ext = '.h5'
dataset_class = 'bin'
dataset_path = '../datasets/hdf5/'

learning_rate = 0.05


def train(is_binary=True):
    """
    trains models (binary and multi class)

    :param is_binary: if true then trains binary class model else trains multi class model
    :return:
    """

    global dataset_class

    checkpoint_path = '../checkpoints/'
    weight_path = '../weights/'
    log_path = '../logs/'
    loss = 'categorical_crossentropy'
    include_top = True
    num_classes = 2

    if not is_binary:
        dataset_class = 'multi'
        num_classes = 12

    dataset_train_path = dataset_path + dataset_class + '/' + file_prefix + '_' + dataset_class + '_train' + file_ext
    dataset_validation_path = dataset_path + dataset_class + '/' + file_prefix + '_' + dataset_class + '_validation' + file_ext
    checkpoint_path += dataset_class + '/' + file_prefix + '_' + dataset_class + '_checkpoint_{epoch:02d}_{loss:.2f}' + file_ext
    weight_path += dataset_class + '/' + file_prefix + '_' + dataset_class + '_weights' + file_ext
    log_path += dataset_class + '/'

    # build model and load weights
    # model = VGG16(weights=None, include_top=include_top, input_shape=(224, 224, 3), classes=num_classes)
    model = SDDFCN(num_class=num_classes, include_top=include_top)  # build model

    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])
    model.summary()

    x_train = HDF5Matrix(dataset_train_path, 'image/encoded')
    y_train = HDF5Matrix(dataset_train_path, 'image/class/label')
    y_train = to_categorical(y_train, num_classes)  # one-hot encoding
    x_validation = HDF5Matrix(dataset_validation_path, 'image/encoded')
    y_validation = HDF5Matrix(dataset_validation_path, 'image/class/label')
    y_validation = to_categorical(y_validation, num_classes)  # one-hot encoding

    # callbacks
    # checkpoint
    checkpoint = ModelCheckpoint(checkpoint_path, monitor='val_acc', verbose=1, save_best_only=True, mode='max')
    # early_stopping
    early_stopping = EarlyStopping(monitor='loss', patience=5)
    # logging
    history = TensorBoard(log_dir=log_path, histogram_freq=0, write_graph=True, write_images=True)
    model.fit(x_train, y_train, batch_size=64, epochs=1,
              validation_data=(x_validation, y_validation), callbacks=[history, checkpoint, early_stopping],
              shuffle='batch')

    model.save_weights(weight_path)  # save weights


train(is_binary=True)
