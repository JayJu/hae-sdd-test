# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. 
2. 
"""
import tensorflow as tf
import tensorflow.contrib.slim as slim
from datasets import coil as sdd_record_dataset
from utils import dataset_utils
from models import vgg16conv as vgg

# parameter
tfrecords_path = '../tfrecords/bin'
dataset_name = 'sdd'
num_classes = 2
batch_size = 32
img_width = img_height = 224
split_name = 'validation'
checkpoint_dir = '../checkpoints/bin/train'
log_dir = '../checkpoints/bin/eval'


def evaluate(is_binary=True):

    tf.logging.set_verbosity(tf.logging.INFO)

    # load dataset
    tfrecord_dataset = sdd_record_dataset.TFRecordDataset(tfrecords_path=tfrecords_path,
                                                          dataset_name=dataset_name,
                                                          num_classes=num_classes)

    # selects the 'train' dataset.
    dataset = tfrecord_dataset.get_split(split_name=split_name)
    images, labels, filenames, count = dataset_utils.load_batch(dataset, batch_size=batch_size,
                                                                height=img_height, width=img_width, is_training=False)

    # label: one-hot encoding
    labels = slim.one_hot_encoding(labels, num_classes)

    # make the model - vgg16
    with slim.arg_scope(vgg.vgg_arg_scope()):
        logits, end_points = vgg.vgg_16(inputs=images, num_classes=num_classes, is_training=False)

    predictions = tf.argmax(logits, 1)
    labels = tf.argmax(labels , 1)
    # mislabeled = tf.not_equal(predictions, labels)
    # mislabeled_filenames = tf.boolean_mask(filenames, mislabeled)

    # Define the metrics:
    names_to_values, names_to_updates = slim.metrics.aggregate_metric_map({
        'eval/Accuracy': tf.metrics.accuracy(predictions, labels),
        # 'eval/Recall@5': slim.metrics.streaming_recall_at_k(logits, labels, 5),
        # 'eval/Recall@3': slim.metrics.streaming_sparse_recall_at_k(tf.to_float(logits), tf.expand_dims(labels, 1), 3),
        # 'eval/Precision': slim.metrics.streaming_precision(predictions, labels),
        # 'eval/Recall': slim.metrics.streaming_recall(predictions, labels)
        # 'eval/filename': filenames,
        # 'eval/prediction': tf.constant(predictions).eval(),
        # 'eval/label': labels
    })

    eval_op = tf.Print(list(names_to_updates.values()), [labels], message="labels: ", summarize=100)
    eval_op = tf.Print(eval_op, [predictions], message='predictions: ', summarize=100)
    eval_op = tf.Print(eval_op, [filenames], message='filename: ', summarize=100)

    # Create the summary ops such that they also print out to std output:
    summary_ops = []
    for metric_name, metric_value in names_to_values.items():
        op = tf.summary.scalar(metric_name, metric_value)
        op = tf.Print(op, [metric_value], metric_name)
        summary_ops.append(op)

    # Setup the global step.
    tf.train.get_or_create_global_step()

    print('Running evaluation Loop...')
    num_evals = count
    eval_interval_secs = 1

    # evaluation_loop need checkpoint_dir, evaluation_once need a checkpoint file
    # checkpoint_path = tf.train.latest_checkpoint(checkpoint_dir)
    metric_values = slim.evaluation.evaluation_loop(
        '',
        checkpoint_dir=checkpoint_dir,
        logdir=log_dir,
        num_evals=num_evals,
        eval_op=eval_op,
        summary_op=tf.summary.merge(summary_ops),
        eval_interval_secs=eval_interval_secs)

    names_to_values = dict(zip(names_to_values.keys(), metric_values))
    for name in names_to_values:
        print('%s: %f' % (name, names_to_values[name]))


evaluate(is_binary=True)
