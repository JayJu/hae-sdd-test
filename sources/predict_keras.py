# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. distinguishes whether the input image has defects of not
2. detects cases of defects through the input image

"""

import sys
import numpy as np
from tensorflow.python.keras.optimizers import Adam
# from tensorflow.python.keras.applications import VGG16
from models.sdd import SDDFCN
import convert_dataset

file_prefix = 'sdd'
file_ext = '.h5'
file_path = None
dataset_class = 'bin'

learning_rate = 0.01


def predict(is_binary=True, img_path=None):
    """
    does the predictions (binary or multi class classification)
    :param is_binary: if true then returns if there is defects or not else returns categories of defects
    :param img_path:
    :return:
    """

    global dataset_class, file_path

    weight_path = '../weights/'
    log_path = '../logs/'

    if img_path:
        file_path = img_path
    loss = 'categorical_crossentropy'
    include_top = True
    num_classes = 2

    if not is_binary:
        dataset_class = 'multi'
        num_classes = 12

    weight_path += dataset_class + '/' + file_prefix + '_' + dataset_class + '_weights' + file_ext
    log_path += dataset_class + '/'

    # build model and load weights
    # model = VGG16(weights=None, include_top=include_top, input_shape=(224, 224, 3), classes=num_classes)
    model = SDDFCN(num_class=num_classes, include_top=include_top)

    print(f'loading weights: {weight_path}')
    model.load_weights(weight_path)
    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer, loss=loss)
    # model.summary()

    x_data = convert_dataset.load_image(file_path)
    x_data = np.expand_dims(x_data, axis=0)
    y_predict = model.predict(x_data)
    # y_class = np.argmax(y_predict)

    print(f'''
    {'='*60}
     * classification: {dataset_class}
     * x: {file_path}
     * y: {y_predict}
    {'='*60}
    ''')

    return y_predict


def check_args(args):
    """
    checks arguments are valid

    :param args:
    :return:
    """
    global dataset_class, file_path

    if not len(args) == 3:
        print(f'''
        Please check your input arguments.
        Usage: predict_keras.py b|m image_path
        ''')
        exit()

    if args[1] == 'b':
        dataset_class = 'bin'
    elif args[1] == 'm':
        dataset_class = 'multi'
    else:
        print(f'''
        Input error..
        index: 1, need: 'b' or 'm', found: {args[1]}
        ''')
        exit()

    if '.jp' in args[2]:
        file_path = args[2]
    else:
        print(f'''
        Input error..
        index: 2, need: *.jpeg|*.jpg, found: {args[2]}
        ''')
        exit()

    return True


# if check_args(sys.argv):
#     predict()
