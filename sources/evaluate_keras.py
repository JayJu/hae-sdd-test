# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. evaluates test datasets with trained weights (binary class and multi class)
"""

from keras.utils.io_utils import HDF5Matrix
from tensorflow.python.keras.utils import to_categorical
from tensorflow.python.keras.optimizers import Adam
# from tensorflow.python.keras.applications import VGG16
from models.sdd import SDDFCN

file_prefix = 'sdd'
file_ext = '.h5'
dataset_class = 'bin'
dataset_path = '../datasets/hdf5/'

learning_rate = 0.01


def evaluate(is_binary=True):
    """
    evaluates test datasets

    :param is_binary:
    :return:
    """

    global dataset_class

    weight_path = '../weights/'
    log_path = '../logs/'
    loss = 'categorical_crossentropy'
    include_top = True
    num_classes = 2

    if not is_binary:
        dataset_class = 'multi'
        num_classes = 12

    weight_path += dataset_class + '/' + file_prefix + '_' + dataset_class + '_weights' + file_ext
    dataset_evaluation_path = dataset_path + dataset_class + '/' + file_prefix + '_' + dataset_class + '_test' + file_ext
    log_path += dataset_class + '/'

    # build model and load weights
    # model = VGG16(weights=None, include_top=include_top, input_shape=(224, 224, 3), classes=num_classes)
    model = SDDFCN(num_class=num_classes, include_top=include_top)  # build model

    model.load_weights(weight_path)
    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])

    x_test = HDF5Matrix(dataset_evaluation_path, 'image/encoded')
    y_test = HDF5Matrix(dataset_evaluation_path, 'image/class/label')
    y_test = to_categorical(y_test, num_classes)
    scores = model.evaluate(x_test, y_test)

    print(f'loss: {scores[0]:.2f}, accuracy: {scores[1]:.2f}')


evaluate(is_binary=False)
