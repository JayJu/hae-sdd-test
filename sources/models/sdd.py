# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. represents user-defined cnn model for detecting defects 
"""

import tensorflow as tf
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, Flatten, Dropout, \
    GlobalAveragePooling2D, BatchNormalization


class SDDFCN:
    learning_rate = 0.0003
    dropout_keep_prob = 0.2

    def __init__(self, num_class=1, include_top=False):

        self.model = Sequential()
        self.build_model(num_class, include_top)

    def build_conv_layer(self, filters, kernel_size=3, activation='relu', padding='SAME'):

        model = self.model
        model.add(Conv2D(filters=filters, kernel_size=(kernel_size, kernel_size),
                         activation=activation, padding=padding))

    def build_conv_block(self, layers, filters, kernel_size=3,
                         activation=tf.nn.relu, padding='SAME'):

        model = self.model
        for i in range(layers):
            self.build_conv_layer(filters=filters, kernel_size=kernel_size,
                                  activation=activation, padding=padding)

        model.add(BatchNormalization())
        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        model.add(Dropout(self.dropout_keep_prob))

    def build_model(self, num_class=1, include_top=False):
        model = self.model
        model.add(Conv2D(filters=64, kernel_size=3, activation=tf.nn.relu, padding='SAME',
                         kernel_initializer='he_uniform', bias_initializer='zeros',
                         input_shape=(224, 224, 3)))
        self.build_conv_block(1, 64)
        self.build_conv_block(2, 128)
        self.build_conv_block(3, 256)
        self.build_conv_block(3, 512)

        if include_top:
            self.build_conv_block(1, 4096, kernel_size=1)
            self.build_conv_block(1, num_class, kernel_size=1, activation=tf.nn.softmax, padding='VALID')
            model.add(GlobalAveragePooling2D())
            model.add(Flatten())

    def summary(self):
        model = self.model
        model.summary()

    def compile(self, optimizer, loss='categorical_crossentropy', metrics=['accuracy']):
        model = self.model
        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def fit_generator(self, train_generator, steps_per_epoch=100, epochs=10, validation_data=None,
                      validation_steps=50, callbacks=None):
        model = self.model
        model.fit_generator(generator=train_generator, steps_per_epoch=steps_per_epoch,
                            epochs=epochs, validation_data=validation_data,
                            validation_steps=validation_steps,
                            callbacks=callbacks)

    def fit(self, data, labels, batch_size=32, epochs=1, validation_data=None, callbacks=None, shuffle='batch'):
        model = self.model
        model.fit(x=data, y=labels, batch_size=batch_size, epochs=epochs, validation_data=validation_data,
                  callbacks=callbacks, shuffle=shuffle)

    def evaluate(self, data, labels, batch_size=32, verbose=1, steps=None):
        model = self.model
        scores = model.evaluate(data, labels, batch_size=batch_size, verbose=verbose, steps=steps)

        return scores

    def predict(self, data):
        model = self.model
        predicted = model.predict_classes(x=data)

        return predicted

    def load_weights(self, path=None):
        model = self.model
        model.load_weights(filepath=path)

    def save_weights(self, path=None):
        model = self.model
        model.save_weights(path)


