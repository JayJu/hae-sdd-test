# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

import tensorflow as tf
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Conv2D, MaxPooling2D, Flatten, Dropout, GlobalAveragePooling2D
from tensorflow.python.keras.losses import binary_crossentropy
from tensorflow.python.keras import regularizers


class Vgg16FCN:
    learning_rate = 0.0003
    dropout_keep_prob = 0.5
    weight_decay = 0.0005

    def __init__(self, num_class=1, include_top=False, checkpoint_path=None):

        self.model = Sequential()
        self.build_model(num_class, include_top, checkpoint_path)

    def build_conv_layer(self, filters, kernel_size=3, activation=tf.nn.relu, padding='SAME',
                         kernel_initializer='he_uniform', baias_initializer='zeros'):

        model = self.model
        model.add(Conv2D(filters=filters, kernel_size=(kernel_size, kernel_size),
                         activation=activation, padding=padding,
                         kernel_initializer=kernel_initializer,
                         bias_initializer=baias_initializer,
                         kernel_regularizer=regularizers.l2(self.weight_decay)))

    def build_conv_block(self, layers, filters, kernel_size=3,
                         activation=tf.nn.relu, padding='SAME',
                         kernel_initializer='he_uniform',
                         baias_initializer='zeros'):

        model = self.model
        for i in range(layers):
            self.build_conv_layer(filters=filters, kernel_size=kernel_size,
                                  activation=activation, padding=padding,
                                  kernel_initializer=kernel_initializer, baias_initializer=baias_initializer)

        model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    def build_fc_block(self, filters, kernel_size=1, activation=tf.nn.relu, padding='SAME'):
        model = self.model
        self.build_conv_layer(filters=filters, kernel_size=kernel_size, activation=activation, padding=padding)
        model.add(Dropout(self.dropout_keep_prob))

    def build_model(self, num_class=1, include_top=False, checkpoint_path=None):
        model = self.model
        model.add(Conv2D(filters=64, kernel_size=(3, 3), activation=tf.nn.relu, padding='SAME',
                         kernel_initializer='he_uniform', bias_initializer='zeros',
                         input_shape=(224, 224, 3)))
        self.build_conv_block(1, 64)
        self.build_conv_block(2, 128)
        self.build_conv_block(3, 256)
        self.build_conv_block(3, 512)
        self.build_conv_block(3, 512)

        if include_top:
            self.build_fc_block(4096, 7, padding='VALID')
            self.build_fc_block(4096, 1)
            self.build_fc_block(num_class, 1, activation=tf.nn.sigmoid if num_class == 1 else tf.nn.softmax, padding='VALID')
            model.add(GlobalAveragePooling2D())
            model.add(Flatten())

        self.summary()

    def summary(self):
        model = self.model
        model.summary()

    def compile(self, optimizer, loss='categorical_crossentropy', metrics=['accuracy']):
        model = self.model
        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    def fit_generator(self, train_generator, steps_per_epoch=100, epochs=10, validation_data=None,
                      validation_steps=50, callbacks=None):
        model = self.model
        model.fit_generator(generator=train_generator, steps_per_epoch=steps_per_epoch,
                            epochs=epochs, validation_data=validation_data,
                            validation_steps=validation_steps,
                            callbacks=callbacks)

    # def fit(self, data, labels, epochs=1, verbose=1, steps_per_epoch=1, callbacks=None):
    #     model = self.model
    #     model.fit(data, labels, epochs=epochs, verbose=verbose, steps_per_epoch=steps_per_epoch, callbacks=callbacks)

    def fit(self, data, labels, batch_size=10, epochs=1, validation_data=None, callbacks=None, shuffle='batch'):
        model = self.model
        model.fit(x=data, y=labels, batch_size=batch_size, epochs=epochs, validation_data=validation_data,
                  callbacks=callbacks, shuffle=shuffle)

    def evaluate(self, data, labels, verbose=1, steps=1):
        model = self.model
        scores = model.evaluate(data, labels, verbose=verbose, steps=steps)
        # return loss and acc
        return scores

    def predict(self, data):
        model = self.model
        predicted = model.predict_classes(x=data)

        return predicted

    def load_weight(self, path=None):
        model = self.model
        model.load_weights(filepath=path)


