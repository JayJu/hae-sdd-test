import h5py

# num_limit = 100
f = h5py.File('../../datasets/hdf5/bin/sdd_bin_train.h5', 'r')
labels = f['image/class/label']
files = f['image/filename']

# if len(labels) < num_limit:
#     num_limit = len(labels)
num_limit = len(labels)
cnt_norm = 0
norm_list = []
cnt_defect = 0
defect_list = []

for i in range(num_limit):
    if labels[i] == 0:
        cnt_defect += 1
        defect_list.append(files[i])
        print(f'file: {files[i]}, label: {labels[i]}')
    elif labels[i] == 1:
        cnt_norm += 1
        norm_list.append(files[i])
    else:
        print(f'error: no labels - {labels[i]}')


print(f'Total: {num_limit}, Normal: {cnt_norm}, Defect: {cnt_defect}')
