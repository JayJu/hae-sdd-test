# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

from keras.utils.io_utils import HDF5Matrix
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam
from models.vgg16fcn import Vgg16FCN

file_prefix = 'sdd'
file_ext = '.h5'
dataset_class = 'bin'
dataset_path = '../datasets/hdf5/'
checkpoint_path = '../checkpoints/'
log_path = '../logs/'

learning_rate = 0.0003


def evaluate(is_binary=True):
    global dataset_class, log_path, checkpoint_path

    if not is_binary:
        dataset_class = 'multi'

    checkpoint_path += dataset_class + '/' + file_prefix + '_weights.h5'
    dataset_evaluation_path = dataset_path + dataset_class + '/' + file_prefix + '_' + dataset_class + '_test.h5'
    log_path += dataset_class + '/'

    # build model and load weights
    model = Vgg16FCN(include_top=True, checkpoint_path=checkpoint_path)
    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer, loss=binary_crossentropy, metrics=['accuracy'])
    x_test = HDF5Matrix(dataset_evaluation_path, 'image/encoded')
    y_test = HDF5Matrix(dataset_evaluation_path, 'image/class/label')
    scores = model.evaluate(x_test, y_test)
    print(f'loss: {scores[0]:.2f}, accuracy: {scores[1]:.2f}')


evaluate(is_binary=False)
