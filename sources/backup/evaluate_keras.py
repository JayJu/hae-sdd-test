# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

from keras.losses import binary_crossentropy
from keras.optimizers import Adam
import sources.utils.dataset_utils as util
from sources.models.vgg16fcn import Vgg16FCN

learning_rate = 0.0002
dropout_keep_prob = 0.5
weight_decay = 0.0005
test_data_path = '../tfrecords/bin/sdd_validation.tfrecord'


model = Vgg16FCN(include_top=True)
optimizer = Adam(lr=learning_rate)
model.compile(optimizer=optimizer, loss=binary_crossentropy, metrics=['accuracy'])
data, labels = util.load_dataset(test_data_path, perform_shuffle=True, batch_size=32)

scores = model.evaluate(data, labels)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))