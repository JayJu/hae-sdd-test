# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam
import sources.utils.dataset_utils as util
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard
from sources.models.vgg16fcn import Vgg16FCN

train_data_path = '../tfrecords/bin/sdd_train.tfrecord'
checkpoint_path = '../checkpoints/bin/train/sdd-weights-{epoch:02d}-{loss:.2f}.5'
log_path = '../logs'

learning_rate = 0.008
dropout_keep_prob = 0.5
weight_decay = 0.0005

model = Vgg16FCN(include_top=False)
optimizer = Adam(lr=learning_rate)
model.compile(optimizer=optimizer, loss=binary_crossentropy, metrics=['accuracy'])
data, labels = util.load_dataset(train_data_path, perform_shuffle=True, batch_size=32)
# checkpoint
checkpoint = ModelCheckpoint(checkpoint_path, monitor='loss', verbose=1, save_best_only=True, mode='max')
# early_stopping
early_stopping = EarlyStopping(monitor='loss', patience=5)
# loggin
history = TensorBoard(log_dir=log_path, histogram_freq=0, write_graph=True, write_images=True)
model.fit(data, labels, epochs=200, verbose=1, steps_per_epoch=1, callbacks=[checkpoint, history])


