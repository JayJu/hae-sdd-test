# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

import tensorflow as tf
from google.protobuf.json_format import MessageToJson

count = 0
lists = []
for l in tf.python_io.tf_record_iterator("../tfrecords/bin/sdd_validation.tfrecord"):
    lists.append(tf.train.Example.FromString(l))
    count += 1

# jsonMessage = MessageToJson(tf.train.Example.FromString(example))
# print(jsonMessage)
print(f'total count: {count}')