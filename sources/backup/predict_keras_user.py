# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

import sys
import numpy as np
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam
from models.vgg16fcn import Vgg16FCN
import convert_dataset

file_prefix = 'sdd'
file_ext = '.h5'
file_path = None
dataset_class = 'multi'
dataset_path = '../datasets/hdf5/'
checkpoint_path = '../checkpoints/'
log_path = '../logs/'

learning_rate = 0.0003


def predict():
    global dataset_class, log_path, checkpoint_path

    checkpoint_path += dataset_class + '/' + file_prefix + '_weights.h5'
    log_path += dataset_class + '/'

    # build model and load weights
    model = Vgg16FCN(include_top=True, checkpoint_path=checkpoint_path)
    optimizer = Adam(lr=learning_rate)
    model.compile(optimizer=optimizer, loss=binary_crossentropy)
    x_data = convert_dataset.load_image(file_path)
    x_data = np.expand_dims(x_data, axis=0)
    y_predict = model.predict(x_data)
    print(f'''
    {'='*50}
      x: {file_path}, y_predict: {y_predict}
    {'='*50}
    ''')


def check_args(args):
    global dataset_class, file_path

    if not len(args) == 3:
        print(f'''
        Please check your input arguments.
        predict_keras.py b|m image_path
        ''')
        exit()

    if args[1] == 'b':
        dataset_class = 'bin'
    elif args[1] == 'm':
        dataset_class = 'multi'
    else:
        print(f'''
        Input error..
        index: 0, need: 'b' or 'm', found: {args[1]}
        ''')
        exit()

    if '.jp' in args[2]:
        file_path = args[2]
    else:
        print(f'''
        Input error..
        index: 1, need: *.jpeg|*.jpg, found: {args[2]}
        ''')
        exit()

    return True


if check_args(sys.argv):
    predict()



