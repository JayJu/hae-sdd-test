# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

data_path = '../tfrecords/bin/sdd_train.tfrecord'  # address to save the hdf5 file
with tf.Session() as sess:
    feature = {'image/encoded': tf.FixedLenFeature([], tf.string),
               'image/format': tf.FixedLenFeature((), tf.string, default_value='jpg'),
               'image/filename': tf.FixedLenFeature((), tf.string, default_value='jpg'),
               'image/class/label': tf.FixedLenFeature([], tf.int64)}
    # Create a list of filenames and pass it to a queue
    filename_queue = tf.train.string_input_producer([data_path], num_epochs=1)
    # Define a reader and read the next record
    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)
    # Decode the record read by the reader
    features = tf.parse_single_example(serialized_example, features=feature)
    # Convert the image data from string back to the numbers
    image = tf.image.decode_jpeg(features['image/encoded'], channels=3)

    # Cast label data into int32
    # label = tf.cast(features['image/class/label'], tf.int32)
    label = features['image/class/label']
    # Reshape image data into the original shape
    image = tf.reshape(image, [224, 224, 3])

    # Any preprocessing here ...

    # Creates batches by randomly shuffling tensors
    images, labels = tf.train.shuffle_batch([image, label], batch_size=1, capacity=30, num_threads=1, min_after_dequeue=10)
    # Initialize all global and local variables
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init_op)
    # Create a coordinator and run all QueueRunner objects
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    for batch_index in range(5):
        img, lbl = sess.run([images, labels])
        img = img.astype(np.uint8)
        for j in range(1):
            plt.subplot(2, 3, j+1)
            plt.imshow(img[j, ...])
            plt.title('Normal' if lbl[j] == 0 else 'Abnormal')
        plt.show()
    # Stop the threads
    coord.request_stop()

    # Wait for threads to stop
    coord.join(threads)
    sess.close()