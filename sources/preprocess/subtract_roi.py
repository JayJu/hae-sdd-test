# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. subtracts ROI(Region Of Interest) from source images
2. removes background noise using OpenCV
"""
import cv2
import numpy as np
import glob
from utils import common_util as util
import matplotlib.pyplot as plt

# img_path = 'd:/01.Biz/08.AI/SDD_data_sample/*/*.jpg'
img_path = 'D:/01.Biz/08.AI/Multi_Class/*/*.jpg'
# roi_path = 'd:/02.Dev/Source/hae-sdd-test/datasets/1st_data/'
roi_path = 'd:/02.Dev/Source/hae-sdd-test/datasets/Multi_Class/'
# img_path = '/workspace/2nd_data/*/*.jpg'
# roi_path = '/home/jupyter1/datasets/2nd_data/'


def get_threshold_of_histogram(img):
    """
    gets threshold between background and foreground
    :param img:
    :return:
    """
    # assume binary threshold might be 10
    offset_threshold = 10
    offset_boundary = 50
    threshold = offset_threshold

    img_m = cv2.medianBlur(img, 9)
    img_m = cv2.GaussianBlur(img_m, (15, 15), sigmaX=30)
    # get a histogram of gray-scaled image
    hist = cv2.calcHist(img_m, [0], None, [256], [0, 256])

    tmp_diff1 = np.diff(np.squeeze(hist[:offset_boundary]))
    tmp_diff2 = np.diff(np.squeeze(hist[offset_boundary:]))

    # if there is a pixel bigger than 10 (offset_threshold) between 0 ~ 50th of histogram array
    # it means there is no background otherwise move the threshold
    if np.squeeze(tmp_diff1).max() > offset_threshold:
        max_dec_diff = np.squeeze(tmp_diff2).max()
        hist_diff = list(tmp_diff2)
        max_dec_index = offset_boundary + hist_diff.index(max_dec_diff)
        threshold = max_dec_index - offset_threshold
        # print(f'max_dec_diff: {max_dec_diff}, max_dec_index: {max_dec_index}, threshold: {threshold}')

    """"# uncomment this block to check histogram
    plt.subplot(221)
    plt.imshow(img, 'gray')
    plt.title('src')
    plt.subplot(222)
    plt.imshow(img_m, 'gray')
    plt.title('median')
    plt.subplot(223)
    plt.plot(hist, color='g')
    plt.axvline(threshold, color='r')
    plt.xlim([0, 256])
    plt.title('histogram')
    plt.show()
    """

    return threshold


def get_foreground(path):
    """
    remove background in a image using opencv

    :param path: image path
    :return: foreground image
    """

    print(f'[Info] - subtract - {path}')
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)

    # assume pixels under 'get_threshold_of_histogram(img)' as background
    ret, thresh = cv2.threshold(img, get_threshold_of_histogram(img), 255, cv2.THRESH_BINARY)
    # remove noise
    kernel = np.ones((4, 4), np.uint8)
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)

    _, contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # loop erosion until the number of contour areas become 1 (1 area)
    while len(contours) > 1:
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_ERODE, kernel)
        _, contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    if len(contours) == 0:
        print(f'[Error] - subtract - {path} - size of contours is 0')
        roi = img
    else:
        cnt = contours[0]

        # contour perimeter (외곽선 구하기)
        epsilon = 0.02*cv2.arcLength(cnt, True)
        # contour approximation
        approx = cv2.approxPolyDP(cnt, epsilon, True)

        if len(approx) != 4:
            print(f'[Warn] - subtract - The roi of image is not a shape of rectangle: {len(approx)} - {path}')

        # get roi area
        x, y, w, h = cv2.boundingRect(cnt)
        roi = img[y:y+h, x:x+w]

    return img, roi


def get_split_boundaries(img_bin):
    """
    gets a binary image and split it vertically into two images if there are detects more than 2
    :param img_bin:
    :return: image array with splitted images
    """
    boundary_arr = []
    offset_max_index = 50  # max threshold to split image vertically
    max_index = 0
    start_index = 0
    end_index = 0
    height = img_bin.shape[0]
    width = img_bin.shape[1]

    for y in range(img_bin.shape[1]):
        is_changed = False

        for x in range(img_bin.shape[0]):
            if img_bin[x][y] > 0:
                is_changed = True

        if not is_changed:
            end_index += 1
            if end_index > max_index:
                max_index = end_index
                start_index = y
        else:
            end_index = 0

    start_index -= max_index
    # print(f'start_index: {start_index}, max_index: {max_index}, height: {height}')

    if max_index > offset_max_index:
        boundary_arr.append((0, start_index, height))
        boundary_arr.append((start_index + max_index, width, height))
    else:
        boundary_arr.append(0, width, height)

    return boundary_arr


def get_rois(path):
    rois = []
    offset_threshold1 = 50
    offset_threshold2 = 100
    img_s = cv2.imread(path, cv2.IMREAD_COLOR)
    img_g = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    img_m = cv2.medianBlur(img_g, 3)
    hist = cv2.calcHist(img_m, [0], None, [256], [0, 256])
    img_l = cv2.Laplacian(img_m, cv2.CV_64F, ksize=7, scale=-5)
    img_c = cv2.Canny(img_m, offset_threshold1, offset_threshold2)
    img_inv = cv2.bitwise_not(img_c)

    boundaries = get_split_boundaries(img_c)

    for boundary in boundaries:
        x, w, h = boundary
        img_copy = img_s.copy()
        rois.append(img_copy[0:h, x:x+w])
        cv2.rectangle(img_s, (x, 0), (x+w, h), (255, 0, 0), 2)

    # uncomment this block to check histogram
    plt.figure(figsize=(12, 10))
    plt.subplot(521)
    plt.imshow(img_s)
    plt.title('src')
    plt.subplot(522)
    plt.imshow(img_m, 'gray')
    plt.title('Median')
    plt.subplot(523)
    plt.imshow(img_l, 'gray')
    plt.title('Laplacian')
    plt.subplot(524)
    plt.imshow(img_c, 'gray')
    plt.title('Canny')
    plt.subplot(525)
    plt.plot(hist, color='g')
    plt.axvline(10, color='r')
    plt.xlim([0, 256])
    plt.title('histogram')
    plt.subplot(526)
    plt.imshow(img_inv, 'gray')
    plt.title('Invert')
    plt.subplot(527)
    plt.imshow(rois[0], 'gray')
    plt.title('roi')
    plt.subplot(528)
    plt.imshow(rois[1], 'gray')
    plt.title('roi')
    plt.show()

    return rois, img_s


def remove_background():
    """

    :return:
    """
    addrs = glob.glob(img_path)
    count = 0
    for i in range(len(addrs)):
        addr = addrs[i].replace('\\', '/')
        src, roi = get_foreground(addr)
        dir_path = roi_path + addr.split('/')[-2] + '/'
        util.make_dir_if_not_exists(dir_path)
        roi_name = dir_path + addr.split('/')[-1]
        """# uncomment this block to validate images one by one
        cv2.imshow('src', src)
        cv2.imshow('roi', roi)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        """
        cv2.imwrite(roi_name, roi)
        count += 1

    print('*'*100)
    print(f'[Info] - subtract - total: {len(addrs)}, done: {count}')
    print('*'*100)


# remove_background()
# get_rois('../../datasets/02-24-sample.jpg')
# img = '../../datasets//P_01141025_01__0819.jpg'
# get_foreground(img)
