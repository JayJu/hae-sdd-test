# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. augments images to supplement lack of data
"""

import cv2
import numpy as np
import glob
import utils.common_util as util
import matplotlib.pyplot as plt
import convert_dataset as cvt
from math import ceil, floor

# img_path = 'D:/02.Dev/Source/hae-sdd-test/datasets/1st_data/*[!00]*/*.jpg'
img_path = '../../datasets/Multi_Class/*/*.jpg'
# img_path = '../../datasets/*_data/*[!00]*/*.jpg'
# img_path = '/home/jupyter/datasets/1st_data/*[!00]*/*.jpg'
# roi_path = '/home/jupyter1/datasets/2nd_data/'


def get_flipped_image(img_src, path, is_horizontal=True, is_vertical=False):
    """
    flips images width axes
    :param path:
    :param img_src:
    :param is_horizontal:
    :param is_vertical:
    :return: flipped image, postfix 'h' means horizontal, 'v' means vertical
    """
    img_flip = img_src.copy()
    tmp_list = util.get_file_name(path).split('.')
    img_name_flip = tmp_list[0] + '_flip_'

    if is_horizontal:
        img_flip = cv2.flip(img_flip, 0)
        img_name_flip += 'h'

    if is_vertical:
        img_flip = cv2.flip(img_flip, 1)
        img_name_flip += 'v'

    img_name_flip += '.' + tmp_list[1]

    return img_flip, img_name_flip


def get_scaled_image(img_src, path, beta):
    """
    changes scale of images
    it gets brighter or darker with param 'beta'
    :param img_src:
    :param path:
    :param beta: the amount of changes
    :return: scaled image
    """
    img_scaled = img_src.copy()
    tmp_list = util.get_file_name(path).split('.')
    img_name_scaled = tmp_list[0] + '_scaled_'

    cv2.convertScaleAbs(img_scaled, img_scaled, alpha=1, beta=beta)

    if beta < 0:
        img_name_scaled += str(beta*-1) + 'm'
    else:
        img_name_scaled += str(beta)

    img_name_scaled += '.' + tmp_list[1]

    return img_scaled, img_name_scaled


def get_mean_image(img_src):
    """
    gets a mean image
    :param img_src:
    :return:
    """
    mean = np.array([123.68, 116.779, 103.939], dtyp=np.float32).reshape(3, 1, 1)

    return img_src - mean


def get_translate_parameters(index):
    """
    test function for cropping image
    :param index:
    :return:
    """
    image_size = 224
    if index == 0: # Translate left 20 percent
        offset = np.array([0.0, 0.2], dtype=np.float32)
        size = np.array([image_size, ceil(0.8 * image_size)], dtype=np.int32)
        w_start = 0
        w_end = int(ceil(0.8 * image_size))
        h_start = 0
        h_end = image_size
    elif index == 1:  # Translate right 20 percent
        offset = np.array([0.0, -0.2], dtype=np.float32)
        size = np.array([image_size, ceil(0.8 * image_size)], dtype=np.int32)
        w_start = int(floor((1 - 0.8) * image_size))
        w_end = image_size
        h_start = 0
        h_end = image_size
    elif index == 2:  # Translate top 20 percent
        offset = np.array([0.2, 0.0], dtype=np.float32)
        size = np.array([ceil(0.8 * image_size), image_size], dtype=np.int32)
        w_start = 0
        w_end = image_size
        h_start = 0
        h_end = int(ceil(0.8 * image_size))
    else:  # Translate bottom 20 percent
        offset = np.array([-0.2, 0.0], dtype=np.float32)
        size = np.array([ceil(0.8 * image_size), image_size], dtype=np.int32)
        w_start = 0
        w_end = image_size
        h_start = int(floor((1 - 0.8) * image_size))
        h_end = image_size

    return offset, size, w_start, w_end, h_start, h_end


def translate_images(img_src):
    """
    test function for cropping image
    :param img_src:
    :return:
    """
    image_size = 224
    offsets = np.zeros((len(img_src), 2), dtype = np.float32)
    n_translations = 4
    img_translated_arr = []

    for i in range(n_translations):
        img_translated = np.zeros((len(img_src), image_size, image_size, 3), dtype=np.float32)
        img_translated.fill(1.0) # Filling background color
        base_offset, size, w_start, w_end, h_start, h_end = get_translate_parameters(i)
        offsets[:, :] = base_offset
        # glimpses = tf.image.extract_glimpse(img_src, size, offsets)
        glimpses = img_src[h_start: h_end, w_start: w_end]
        img_translated[:, h_start: h_start + size[0], w_start: w_start + size[1], :] = glimpses
        print(img_translated.shape)
        plt.figure(figsize=(12, 10))
        plt.subplot(111)
        plt.imshow(img_translated[1])
        plt.title('ttt')
        plt.show()

        img_translated_arr.extend(img_translated)
    img_translated_arr = np.array(img_translated_arr, dtype=np.float32)
    return img_translated_arr


def augment_data():
    """
    handles the whole augmentation functions
    it augments images and save them
    :return:
    """
    addrs = glob.glob(img_path)
    count = 0
    scale_ranges = 2
    for i in range(len(addrs)):
        addr = addrs[i].replace('\\', '/')
        img_src = cvt.load_image(addr)
        img_flipped_h, name_flipped_h = get_flipped_image(img_src, addr, is_horizontal=True, is_vertical=False)
        img_flipped_v, name_flipped_v = get_flipped_image(img_src, addr, is_horizontal=False, is_vertical=True)
        img_flipped_hv, name_flipped_hv = get_flipped_image(img_src, addr, is_horizontal=True, is_vertical=True)

        dir_path = '/'.join(addr.split('/')[:-1]) + '/'
        util.make_dir_if_not_exists(dir_path)

        for scale in range(scale_ranges):
            img_scaled_p, name_scaled_p = get_scaled_image(img_src, addr, (scale+1)*3)
            img_scaled_m, name_scaled_m = get_scaled_image(img_src, addr, (scale+1)*-3)
            cv2.imwrite(dir_path+name_scaled_p, img_scaled_p)
            cv2.imwrite(dir_path+name_scaled_m, img_scaled_m)
            print(f'[Info] - augmentation - dir: {dir_path}, name_p: {name_scaled_p}, name_m: {name_scaled_m}')

        """# uncomment this block to validate images one by one
        plt.figure(figsize=(12, 10))
        plt.subplot(221)
        plt.imshow(img_src)
        plt.title(util.get_file_name(addr))
        plt.subplot(222)
        plt.imshow(img_flipped_h)
        plt.title(name_flipped_h)
        plt.subplot(223)
        plt.imshow(img_flipped_v)
        plt.title(name_flipped_v)
        plt.subplot(224)
        plt.imshow(img_flipped_hv)
        plt.title(name_flipped_hv)
        plt.show()
        """
        cv2.imwrite(dir_path+name_flipped_h, img_flipped_h)
        cv2.imwrite(dir_path+name_flipped_v, img_flipped_v)
        cv2.imwrite(dir_path+name_flipped_hv, img_flipped_hv)

        print(f'[Info] - augmentation - dir: {dir_path}, '
              f'name_h: {name_flipped_h}, name_v: {name_flipped_v}, name_hv: {name_flipped_hv}')
        count += 1

    print('*'*100)
    print(f'''
    [Info] - augmentation - source: {count}, augmented: {(count*scale_ranges) + (count*3)} (flipped: {count*3}, scaled: {count*scale_ranges})'
    ''')
    print('*'*100)


# augment_data()
