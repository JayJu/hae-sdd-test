import tensorflow as tf
import tensorflow.contrib.slim as slim


def load_batch(dataset, batch_size=32, width=224, height=224,  num_classes=2, is_training=True):

    """Loads a single batch of data.

    Args:
      dataset: The dataset to load.
      height: The height of the image after preprocessing.
      width: The width of the image after preprocessing.
      batch_size: The number of images in the batch.

    Returns:
      images: A Tensor of size [batch_size, height, width, 3], image samples that have been preprocessed.
      labels: A Tensor of size [batch_size], whose values range between 0 and dataset.num_classes.
      :param is_training:
    """
    # Creates a TF-Slim DataProvider which reads the dataset in the background during both training and testing.
    provider = slim.dataset_data_provider.DatasetDataProvider(dataset)
    [image, label, filename] = provider.get(['image', 'label', 'filename'])

    # image: resize with crop
    #image = tf.image.resize_image_with_crop_or_pad(image, height, width)
    #image = tf.to_float(image)

    image.set_shape([height, width, 3])
    image = tf.to_float(image)
    # label: one-hot encoding
    # one_hot_labels = slim.one_hot_encoding(label, num_classes)

    # Batch it up.
    images, labels, filenames = tf.train.batch(
        [image, label, filename],
        batch_size=batch_size,
        num_threads=1,
        capacity=2 * batch_size)

    return images, labels, filenames, dataset.num_samples


def load_dataset(filenames, perform_shuffle=False, read_count=-1, batch_size=1, image_size=224):
    def _parse_function(serialized):
        features = \
            {
                'image/encoded': tf.FixedLenFeature((), tf.string),
                'image/class/label': tf.FixedLenFeature([], tf.int64)
            }
        # Parse the serialized data so we get a dict with our data.
        parsed_example = tf.parse_single_example(serialized=serialized,
                                                 features=features)
        # Get the image as raw bytes.
        image_encoded = parsed_example['image/encoded']
        # Decode the raw bytes so it becomes a tensor with type.
        image_raw = tf.image.decode_jpeg(image_encoded, channels=3)
        label = parsed_example['image/class/label']
        image = tf.reshape(image_raw, [image_size, image_size, 3])
        # image = tf.subtract(image, 116.779) # Zero-center by mean pixel
        # image = tf.reverse(image, axis=[2]) # 'RGB'->'BGR'
        label = tf.reshape(label, (-1,))

        return image, label

    dataset = tf.data.TFRecordDataset(filenames=filenames)
    # Parse the serialized data in the TFRecords files.
    # This returns TensorFlow tensors for the image and labels.
    dataset = dataset.map(_parse_function)

    if perform_shuffle:
        # Randomizes input using a window of 256 elements (read into memory)
        dataset = dataset.shuffle(buffer_size=256)

    dataset = dataset.repeat(read_count)  # Repeats dataset this # times
    dataset = dataset.batch(batch_size)  # Batch size to use
    iterator = dataset.make_one_shot_iterator()
    batch_features, batch_labels = iterator.get_next()

    return batch_features, batch_labels



# tf_path = '../../tfrecords/bin/sdd_train.tfrecord'
# tf_path2 = '../../tfrecords/bin/sdd_test.tfrecord'
# next_batch = load_dataset('../../tfrecords/bin/sdd_train.tfrecord', perform_shuffle=True)
# with tf.Session() as sess:
#     first_batch = sess.run(next_batch)
# x_d = first_batch[0]
#
# print(x_d.shape)
# img = image.array_to_img(x_d)
# print(img)

