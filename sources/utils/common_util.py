# -*- coding: utf-8 -*-
__author__ = 'heejin_ju@hyundai-autoever.com'

"""
This file:
1. supports some common utility functions
"""

import tensorflow as tf


def make_dir_if_not_exists(path):
    """
    makes a directory if there is no directory with the path
    :param path:
    :return:
    """
    if not tf.gfile.Exists(path):
        tf.gfile.MakeDirs(path)


def get_file_name(path):
    """
    subtracts a file name in the path
    :param path:
    :return:
    """
    return path.replace('\\', '/').split('/')[-1]
