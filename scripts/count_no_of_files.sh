total_cnt=0
printf "\n"
for dir in $(find . -maxdepth 1 -mindepth 1 -type d)
do
  printf "%-10s : " "$dir"
  find "$dir" -type f | wc -l
  cnt=`find "$dir" -type f | wc -l`
  #cnt=$((cnt+1))
  total_cnt=$((total_cnt+cnt))
done
printf "\n Total Count: %s \n\n" $total_cnt